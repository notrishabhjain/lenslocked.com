package controllers

import (
	"lenslocked.com/views"
)

// NewStatic is used to creat a new Static controller.
// This function will panic if the templates are not
// parsed correctly, and should only be used during
// initial setup.
func NewStatic() *Static {
	return &Static{
		HomeView:    views.NewView("bootstrap", "static/home"),
		ContactView: views.NewView("bootstrap", "static/contact"),
	}
}

// Static now has 2 field
type Static struct {
	HomeView    *views.View
	ContactView *views.View
}
