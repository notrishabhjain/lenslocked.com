package context

import (
	"context"

	"lenslocked.com/models"
)

const (
	userKey priveteKey = "user"
)

type priveteKey string

// WithUser ...
func WithUser(ctx context.Context, user *models.User) context.Context {
	return context.WithValue(ctx, userKey, user)
}

// User ...
func User(ctx context.Context) *models.User {
	if temp := ctx.Value(userKey); temp != nil {
		if user, ok := temp.(*models.User); ok {
			return user
		}
	}
	return nil
}
